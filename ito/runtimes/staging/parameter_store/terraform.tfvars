# 本来これら秘匿情報はパブリックリポジトリで公開されるべき内容ではありません。
# そのため、特に重要なパスワードなどは伏字としています。参考程度に捉えてください。
nablarch_db_url                           = "jdbc:postgresql://ito-rds-instance.cxo8wqwdlgfn.ap-northeast-1.rds.amazonaws.com:5432/ito_postgres"
nablarch_db_user                          = "xxxxxxx"
nablarch_db_password                      = "xxxxxxx"
nablarch_db_schema                        = "public"
websocket_url                             = "wss://ito-chat-example-notifier.staging.epona-handson.com/notification"
mail_smtp_host                            = "email-smtp.ap-northeast-1.amazonaws.com"
mail_smtp_port                            = 587
mail_smtp_user                            = "XXXXXX"
mail_smtp_password                        = "XXXXXX"
mail_from_address                         = "XXXXXX@xxxx.com"
mail_returnpath                           = "XXXXXX@xxxx.com"
application_external_url                  = "https://ito-chat-example.staging.epona-handson.com"
cors_origins                              = "https://ito-chat-example.staging.epona-handson.com"
nablarch_sessionstorehandler_cookiesecure = "true"
nablarch_lettuce_simple_url               = "rediss://redis-xxxx@ito-redis-group-001.ito-redis-group.0mgfiw.apne1.cache.amazonaws.com:6379"
# datadog_api_key                           = "xxxxxx"
